FROM hairyhenderson/gomplate:v3.11.7-slim@sha256:0a08ab8b9a4f06b8abbf7df8ba1acf3c36fec7be940f0281b48e0b14eaa6ff69 as gomplate

FROM ubuntu:24.04@sha256:72297848456d5d37d1262630108ab308d3e9ec7ed1c3286a32fe09856619a782

# renovate: datasource=repology depName=ubuntu_24_04/ca-certificates versioning=loose
ENV CA_CERTIFICATES_VERSION=20240203
# renovate: datasource=repology depName=ubuntu_24_04/curl versioning=loose
ENV CURL_VERSION=8.5.0-2ubuntu10.6

RUN set -eux; \
      apt-get update -y; \
      DEBIAN_FRONTEND=noninteractive \
      apt-get install -y --no-install-recommends \
        "ca-certificates=${CA_CERTIFICATES_VERSION}" \
        "curl=${CURL_VERSION}" \
      ; \
      apt-get autoremove --purge -y; \
      apt-get clean -y; \
      rm -rf \
        /var/lib/apt \
        /var/lib/cache \
        /var/lib/log \
      ;

#hadolint ignore=DL3059
RUN set -eux; \
      curl -L https://github.com/mumble-voip/mumble/releases/download/v1.4.230/mumble-1.4.230.tar.gz > /dev/null; \
      curl -L https://github.com/aristocratos/btop/releases/download/v1.0.9/btop-1.0.9-linux-x86_64.tbz > /dev/null;

COPY --from=gomplate /gomplate /usr/sbin/gomplate
