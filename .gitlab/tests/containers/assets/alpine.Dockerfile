FROM hairyhenderson/gomplate:v3.11.7-slim@sha256:0a08ab8b9a4f06b8abbf7df8ba1acf3c36fec7be940f0281b48e0b14eaa6ff69 as gomplate

FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

# renovate: datasource=repology depName=alpine_3_21/ca-certificates versioning=loose
ENV CA_CERTIFICATES_VERSION=20241121-r1
# renovate: datasource=repology depName=alpine_3_21/curl versioning=loose
ENV CURL_VERSION=8.12.1-r0

RUN apk add --no-cache \
      "ca-certificates=${CA_CERTIFICATES_VERSION}" \
      "curl=${CURL_VERSION}" \
    ;

#hadolint ignore=DL3059
RUN set -eux; \
      curl -L https://github.com/mumble-voip/mumble/releases/download/v1.4.230/mumble-1.4.230.tar.gz > /dev/null; \
      curl -L https://github.com/aristocratos/btop/releases/download/v1.0.9/btop-1.0.9-linux-x86_64.tbz > /dev/null;

COPY --from=gomplate /gomplate /usr/sbin/gomplate
